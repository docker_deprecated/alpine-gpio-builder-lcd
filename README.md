# alpine-gpio-builder-lcd

#### [alpine-aarch64-gpio-builder-lcd](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-gpio-builder-lcd/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpineaarch64build/alpine-aarch64-gpio-builder-lcd/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpineaarch64build/alpine-aarch64-gpio-builder-lcd/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpineaarch64build/alpine-aarch64-gpio-builder-lcd/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64build/alpine-aarch64-gpio-builder-lcd)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64build/alpine-aarch64-gpio-builder-lcd)
#### [alpine-armhf-gpio-builder-lcd](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-gpio-builder-lcd/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinearmhfbuild/alpine-armhf-gpio-builder-lcd/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinearmhfbuild/alpine-armhf-gpio-builder-lcd/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinearmhfbuild/alpine-armhf-gpio-builder-lcd/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhfbuild/alpine-armhf-gpio-builder-lcd)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhfbuild/alpine-armhf-gpio-builder-lcd)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : aarch64,armhf
* Appplication : LCD Clock
    - LCD Clock



----------------------------------------
#### Run

* Nothing



----------------------------------------
#### Usage

* Nothing



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

